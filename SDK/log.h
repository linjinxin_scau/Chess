#ifndef __LOG_H__
#define __LOG_H__
/**
*
* 打印日志
* 描述：提供打印各种日志功能
* 作者：林锦新
* 日期：2016年2月19日
*/

void logGameMsg(const char* msg, const char* filePath = nullptr, const char* fileName = nullptr);
void logErrorMsg(const char* msg, const  char* filePath = nullptr, const char* fileName = nullptr);

#endif // !__LOG_H__

