#include "RuleCenter.h"
#include <stdlib.h>

RuleCenter::RuleCenter()
{
}

RuleCenter::~RuleCenter()
{
}

/*
* 查找棋子能落子的位置
*/
void RuleCenter::findCallFallPosition(Stc_Piece * piece)
{
	memset(piece->canFallPos, 255, sizeof(piece->canFallPos));

	unsigned char x = piece->logicPos[0];
	unsigned char y = piece->logicPos[1];
	vector<unsigned char> tempPos;

	switch (piece->type)
	{
	case PI_JIANG:	//将
	{
		//往上
		if (y + 1 < 4)
		{
			tempPos.push_back(x);
			tempPos.push_back(y + 1);
		}
		//往下
		if (y - 1 > 0)
		{
			tempPos.push_back(x);
			tempPos.push_back(y - 1);
		}
		//往左
		if (x + 1 < 7)
		{
			tempPos.push_back(x + 1);
			tempPos.push_back(y);
		}
		//往右
		if (x - 1 > 3)
		{
			tempPos.push_back(x - 1);
			tempPos.push_back(y);
		}
		break;
	}
	case PI_SHI:	//士
	{
		//往右下
		if (x - 1 > 3 && y - 1 > 0)
		{
			tempPos.push_back(x - 1);
			tempPos.push_back(y - 1);
		}
		//往右上
		if (x - 1 > 3 && y + 1 < 4)
		{
			tempPos.push_back(x - 1);
			tempPos.push_back(y + 1);
		}
		//往左下
		if (x + 1 < 7 && y - 1 > 0)
		{
			tempPos.push_back(x + 1);
			tempPos.push_back(y - 1);
		}
		//往左上
		if (x + 1 < 7 && y + 1 < 4)
		{
			tempPos.push_back(x + 1);
			tempPos.push_back(y + 1);
		}
		break;
	}
	case PI_XIANG:	//象
	{
		//往右下
		if (x - 2 > 0 && y - 2 > 0)
		{
			//判断象脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && (x - 1) == (*it)->logicPos[0] && (y - 1) == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && 10 - (x - 1) == (*it)->logicPos[0] && 11 - (y - 1) == (*it)->logicPos[1])
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x - 2);
				tempPos.push_back(y - 2);
			}
		}
		//往右上
		if (x - 2 > 0 && y + 2 < 6)
		{
			//判断象脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && (x - 1) == (*it)->logicPos[0] && (y + 1) == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && 10 - (x - 1) == (*it)->logicPos[0] && 11 - (y + 1) == (*it)->logicPos[1])
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x - 2);
				tempPos.push_back(y + 2);
			}
		}
		//往左下
		if (x + 2 < 10 && y - 2 > 0)
		{
			//判断象脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && (x + 1) == (*it)->logicPos[0] && (y - 1) == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && 10 - (x + 1) == (*it)->logicPos[0] && 11 - (y - 1) == (*it)->logicPos[1])
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x + 2);
				tempPos.push_back(y - 2);
			}
		}
		//往左上
		if (x + 2 < 10 && y + 2 < 6)
		{
			//判断象脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && (x + 1) == (*it)->logicPos[0] && (y + 1) == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && 10 - (x + 1) == (*it)->logicPos[0] && 11 - (y + 1) == (*it)->logicPos[1])
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x + 2);
				tempPos.push_back(y + 2);
			}
		}
		break;
	}
	case PI_MA:	//马
	{
		//往右下偏右
		if (x - 2 > 0 && y - 1 > 0)
		{
			//判断马脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && (x - 1) == (*it)->logicPos[0] && y == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && (x - 1) + (*it)->logicPos[0] == 10 && y + (*it)->logicPos[1] == 11)
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x - 2);
				tempPos.push_back(y - 1);
			}
		}
		//往右下偏下
		if (x - 1 > 0 && y - 2 > 0)
		{
			//判断马脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && x == (*it)->logicPos[0] && (y - 1) == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && x + (*it)->logicPos[0] == 10 && (y - 1) + (*it)->logicPos[1] == 11)
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x - 1);
				tempPos.push_back(y - 2);
			}
		}
		//往右上偏右
		if (x - 2 > 0 && y + 1 < 11)
		{
			//判断马脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && (x - 1) == (*it)->logicPos[0] && y == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && (x - 1) + (*it)->logicPos[0] == 10 && y + (*it)->logicPos[1] == 11)
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x - 2);
				tempPos.push_back(y + 1);
			}
		}
		//往右上偏上
		if (x - 1 > 0 && y + 2 < 11)
		{
			//判断马脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && x == (*it)->logicPos[0] && (y + 1) == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && x + (*it)->logicPos[0] == 10 && (y + 1) + (*it)->logicPos[1] == 11)
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x - 1);
				tempPos.push_back(y + 2);
			}
		}
		//往左下偏左
		if (x + 2 < 10 && y - 1 > 0)
		{
			//判断马脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && (x + 1) == (*it)->logicPos[0] && y == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && (x + 1) + (*it)->logicPos[0] == 10 && y + (*it)->logicPos[1] == 11)
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x + 2);
				tempPos.push_back(y - 1);
			}
		}
		//往左下偏下
		if (x + 1 < 10 && y - 2 > 0)
		{
			//判断马脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && x == (*it)->logicPos[0] && (y - 1) == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && x + (*it)->logicPos[0] == 10 && (y - 1) + (*it)->logicPos[1] == 11)
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x + 1);
				tempPos.push_back(y - 2);
			}
		}
		//往左上偏左
		if (x + 2 < 10 && y + 1 < 11)
		{
			//判断马脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && (x + 1) == (*it)->logicPos[0] && y == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && (x + 1) + (*it)->logicPos[0] == 10 && y + (*it)->logicPos[1] == 11)
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x + 2);
				tempPos.push_back(y + 1);
			}
		}
		//往左上偏上
		if (x + 1 < 10 && y + 2 < 11)
		{
			//判断马脚是否被占
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it)->player == piece->player && x == (*it)->logicPos[0] && (y + 1) == (*it)->logicPos[1])
				{
					break;
				}
				else if ((*it)->player != piece->player && x + (*it)->logicPos[0] == 10 && (y + 1) + (*it)->logicPos[1] == 11)
				{
					break;
				}
			}
			if (it == vecPiece.end())
			{
				tempPos.push_back(x + 1);
				tempPos.push_back(y + 2);
			}
		}
		break;
	}
	case PI_CHE:	//车
	{
		//往右
		for (int i = x - 1; i > 0; i--)
		{
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it) == piece) continue;
				//是对方棋子位置要转换
				if ((*it)->player != piece->player)
				{
					if ((10 - i) == (*it)->logicPos[0] && (11 - y) == (*it)->logicPos[1])
					{
						//是对方子则可以移动到此位置
						tempPos.push_back(i);
						tempPos.push_back(y);
						break;
					}
				}
				else
				{
					if (i == (*it)->logicPos[0] && y == (*it)->logicPos[1])
					{
						break;
					}
				}
			}
			//找到尽头
			if (it != vecPiece.end())
			{
				break;
			}
			tempPos.push_back(i);
			tempPos.push_back(y);
		}
		//往左
		for (int i = x + 1; i < 10; i++)
		{
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it) == piece) continue;
				//是对方棋子位置要转换
				if ((*it)->player != piece->player)
				{
					if ((10 - i) == (*it)->logicPos[0] && (11 - y) == (*it)->logicPos[1])
					{
						//是对方子则可以移动到此位置
						tempPos.push_back(i);
						tempPos.push_back(y);
						break;
					}
				}
				else
				{
					if (i == (*it)->logicPos[0] && y == (*it)->logicPos[1])
					{
						break;
					}
				}
			}
			//找到尽头
			if (it != vecPiece.end())
			{
				break;
			}
			tempPos.push_back(i);
			tempPos.push_back(y);
		}
		//往下
		for (int i = y - 1; i > 0; i--)
		{
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it) == piece) continue;
				//是对方棋子位置要转换
				if ((*it)->player != piece->player)
				{
					if ((10 - x) == (*it)->logicPos[0] && (11 - i) == (*it)->logicPos[1])
					{
						//是对方子则可以移动到此位置
						tempPos.push_back(x);
						tempPos.push_back(i);
						break;
					}
				}
				else
				{
					if (x == (*it)->logicPos[0] && i == (*it)->logicPos[1])
					{
						break;
					}
				}
			}
			//找到尽头
			if (it != vecPiece.end())
			{
				break;
			}
			tempPos.push_back(x);
			tempPos.push_back(i);
		}
		//往上
		for (int i = y + 1; i < 11; i++)
		{
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false) continue;
				if ((*it) == piece) continue;
				//是对方棋子位置要转换
				if ((*it)->player != piece->player)
				{
					if ((10 - x) == (*it)->logicPos[0] && (11 - i) == (*it)->logicPos[1])
					{
						//是对方子则可以移动到此位置
						tempPos.push_back(x);
						tempPos.push_back(i);
						break;
					}
				}
				else
				{
					if (x == (*it)->logicPos[0] && i == (*it)->logicPos[1])
					{
						break;
					}
				}
			}
			//找到尽头
			if (it != vecPiece.end())
			{
				break;
			}
			tempPos.push_back(x);
			tempPos.push_back(i);
		}
		//车不作统一筛选被占位置
		goto L_assign;
	}
	case PI_PAO:	//炮
	{
		//向右
		bool bFindPaoJia = false;	//是否找到炮架
		for (int i = x - 1; i > 0; i--)
		{
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false)
				{
					continue;
				}
				//对方棋子要转换位置
				if ((*it)->player != piece->player)
				{
					if ((10 - i) == (*it)->logicPos[0] && (11 - y) == (*it)->logicPos[1])
					{
						//已经找到炮架了，则该棋子能吃
						if (bFindPaoJia)
						{
							tempPos.push_back(i);
							tempPos.push_back(y);
							bFindPaoJia = false;
							break;
						}
						//还没找到炮架，则该棋子当作炮架
						else
						{
							bFindPaoJia = true;
							break;
						}
					}
				}
				else
				{
					if (i == (*it)->logicPos[0] && y == (*it)->logicPos[1])
					{
						//已经找到炮架了，则该结束
						if (bFindPaoJia)
						{
							bFindPaoJia = false;
							break;
						}
						//还没找到炮架，则该棋子当作炮架
						else
						{
							bFindPaoJia = true;
							break;
						}
					}
				}
			}
			
			if (it != vecPiece.end())
			{
				if (bFindPaoJia) continue;	//找到炮架，则跳过该棋子
				else break;	//找到尽头,退出
			}

			//没找到炮架前的空位置可移动
			if (!bFindPaoJia)
			{
				tempPos.push_back(i);
				tempPos.push_back(y);
			}
		}
		//向左
		bFindPaoJia = false;	//是否找到炮架
		for (int i = x + 1; i < 10; i++)
		{
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false)
				{
					continue;
				}
				//对方棋子要转换位置
				if ((*it)->player != piece->player)
				{
					if ((10 - i) == (*it)->logicPos[0] && (11 - y) == (*it)->logicPos[1])
					{
						//已经找到炮架了，则该棋子能吃
						if (bFindPaoJia)
						{
							tempPos.push_back(i);
							tempPos.push_back(y);
							bFindPaoJia = false;
							break;
						}
						//还没找到炮架，则该棋子当作炮架
						else
						{
							bFindPaoJia = true;
							break;
						}
					}
				}
				else
				{
					if (i == (*it)->logicPos[0] && y == (*it)->logicPos[1])
					{
						//已经找到炮架了，则该结束
						if (bFindPaoJia)
						{
							bFindPaoJia = false;
							break;
						}
						//还没找到炮架，则该棋子当作炮架
						else
						{
							bFindPaoJia = true;
							break;
						}
					}
				}
			}

			if (it != vecPiece.end())
			{
				if (bFindPaoJia) continue;	//找到炮架，则跳过该棋子
				else break;	//找到尽头,退出
			}

			//没找到炮架前的空位置可移动
			if (!bFindPaoJia)
			{
				tempPos.push_back(i);
				tempPos.push_back(y);
			}
		}
		//向下
		bFindPaoJia = false;	//是否找到炮架
		for (int i = y - 1; i > 0; i--)
		{
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false)
				{
					continue;
				}
				//对方棋子要转换位置
				if ((*it)->player != piece->player)
				{
					if ((10 - x) == (*it)->logicPos[0] && (11 - i) == (*it)->logicPos[1])
					{
						//已经找到炮架了，则该棋子能吃
						if (bFindPaoJia)
						{
							tempPos.push_back(x);
							tempPos.push_back(i);
							bFindPaoJia = false;
							break;
						}
						//还没找到炮架，则该棋子当作炮架
						else
						{
							bFindPaoJia = true;
							break;
						}
					}
				}
				else
				{
					if (x == (*it)->logicPos[0] && i == (*it)->logicPos[1])
					{
						//已经找到炮架了，则该结束
						if (bFindPaoJia)
						{
							bFindPaoJia = false;
							break;
						}
						//还没找到炮架，则该棋子当作炮架
						else
						{
							bFindPaoJia = true;
							break;
						}
					}
				}
			}

			if (it != vecPiece.end())
			{
				if (bFindPaoJia) continue;	//找到炮架，则跳过该棋子
				else break;	//找到尽头,退出
			}

			//没找到炮架前的空位置可移动
			if (!bFindPaoJia)
			{
				tempPos.push_back(x);
				tempPos.push_back(i);
			}
		}
		//向上
		bFindPaoJia = false;	//是否找到炮架
		for (int i = y + 1; i < 11; i++)
		{
			vector<Stc_Piece*>::iterator it = vecPiece.begin();
			for (; it != vecPiece.end(); it++)
			{
				if ((*it)->isAlive == false)
				{
					continue;
				}
				//对方棋子要转换位置
				if ((*it)->player != piece->player)
				{
					if ((10 - x) == (*it)->logicPos[0] && (11-i) == (*it)->logicPos[1])
					{
						//已经找到炮架了，则该棋子能吃
						if (bFindPaoJia)
						{
							tempPos.push_back(x);
							tempPos.push_back(i);
							bFindPaoJia = false;
							break;
						}
						//还没找到炮架，则该棋子当作炮架
						else
						{
							bFindPaoJia = true;
							break;
						}
					}
				}
				else
				{
					if (x == (*it)->logicPos[0] && i == (*it)->logicPos[1])
					{
						//已经找到炮架了，则该结束
						if (bFindPaoJia)
						{
							bFindPaoJia = false;
							break;
						}
						//还没找到炮架，则该棋子当作炮架
						else
						{
							bFindPaoJia = true;
							break;
						}
					}
				}
			}

			if (it != vecPiece.end())
			{
				if (bFindPaoJia) continue;	//找到炮架，则跳过该棋子
				else break;	//找到尽头,退出
			}

			//没找到炮架前的空位置可移动
			if (!bFindPaoJia)
			{
				tempPos.push_back(x);
				tempPos.push_back(i);
			}
		}
		//炮不作统一筛选被占位置
		goto L_assign;
	}
	case PI_ZHU:	//卒
	{
		//向上（前）
		if (y + 1 < 11)
		{
			tempPos.push_back(x);
			tempPos.push_back(y + 1);
		}
		//小兵过河后可以左右走
		if (y > 5)
		{
			//向右
			if (x - 1 > 0)
			{
				tempPos.push_back(x - 1);
				tempPos.push_back(y);
			}
			//向左
			if (x + 1 < 10)
			{
				tempPos.push_back(x + 1);
				tempPos.push_back(y);
			}
		}
		break;
	}
	default:
		break;
	}
	//去掉被我方棋子占着的位置
	for(vector<Stc_Piece*>::iterator it=vecPiece.begin();it!=vecPiece.end();it++)
	{
		if ((*it)->isAlive == false)
		{
			continue;
		}
		//对方棋子占着的位置不用去掉
		if ((*it)->player != piece->player)
		{
			continue;
		}

		x = (*it)->logicPos[0];
		y = (*it)->logicPos[1];

		for (vector<unsigned char>::iterator itr = tempPos.begin(); itr != tempPos.end(); itr += 2)
		{
			if (*itr == x && *(itr+1) == y)
			{
				//这里必须先删除后面的元素，否则会崩溃
				tempPos.erase(itr + 1);
				tempPos.erase(itr);
				break;
			}
		}
	}

L_assign:
	//把可落子位置赋给棋子
	int index = 0;
	for (vector<unsigned char>::iterator itr = tempPos.begin(); itr != tempPos.end(); itr += 2)
	{
		piece->canFallPos[index][0] = *itr;
		piece->canFallPos[index][1] = *(itr + 1);
		index++;
	}
}

/*
* 判断是否将军
*/
bool RuleCenter::canJiangJu(Em_Player attack)
{
	auto canJiangPieces = &mapCanJiangPiece[attack];
	canJiangPieces->clear();

	//查找被将方的将军
	Stc_Piece* goal;
	for (vector<Stc_Piece*>::iterator it = vecPiece.begin(); it != vecPiece.end(); it++)
	{
		if ((*it)->player != attack && (*it)->type == PI_JIANG)
		{
			goal = *it;
			break;
		}
	}

	//遍历将军方的棋子
	for (vector<Stc_Piece*>::iterator it = vecPiece.begin(); it != vecPiece.end(); it++)
	{
		if ((*it)->player == attack && (*it)->isAlive)
		{
			switch ((*it)->type)
			{
			case PI_JIANG:
			{
				//将帅相对才能将对方
				if ((*it)->logicPos[0] + goal->logicPos[0] != 10)
				{
					break;
				}
				//判断将帅之间有没有其它棋子隔着
				vector<Stc_Piece*>::iterator itr = vecPiece.begin();
				for (; itr != vecPiece.end(); itr++)
				{
					if (*itr == *it || *itr == goal || !(*itr)->isAlive)
					{
						continue;
					}
					if ((*itr)->logicPos[0] + goal->logicPos[0] == 10)
					{
						break;
					}
				}
				//没有其它棋子隔着
				if (itr == vecPiece.end())
				{
					canJiangPieces->push_back(*it);
				}
				break;
			}
			case PI_SHI:
			case PI_XIANG:
			{
				//士和象不可能将对方
				break;
			}
			case PI_MA:
			{
				//位置转换成将军方的位置
				int vx = (10 - goal->logicPos[0]) - (*it)->logicPos[0];
				int vy = (11 - goal->logicPos[1]) - (*it)->logicPos[1];
				if (abs(vx) == 2 && abs(vy) == 1)
				{
					//保存马脚位置
					vx = (*it)->logicPos[0] + vx / 2;
					vy = (*it)->logicPos[1];
				}
				else if (abs(vx) == 1 && abs(vy) == 2)
				{
					//保存马脚位置
					vx = (*it)->logicPos[0];
					vy = (*it)->logicPos[1] + vy / 2;
				}
				else
				{
					break;
				}
				//判断马脚是否被占
				vector<Stc_Piece*>::iterator itr;
				for (itr = vecPiece.begin(); itr != vecPiece.end(); itr++)
				{
					if (*itr == *it || *itr == goal || !(*itr)->isAlive) continue;
					//将军方不需要转换位置
					if ((*itr)->player == attack)
					{
						if ((*itr)->logicPos[0] == vx && (*itr)->logicPos[1] == vy)
						{
							break;
						}
					}
					//被将军方要转换位置
					else
					{
						if (10 - (*itr)->logicPos[0] == vx && 11 - (*itr)->logicPos[1] == vy)
						{
							break;
						}
					}
				}
				//马脚没有被占
				if (itr == vecPiece.end())
				{
					canJiangPieces->push_back(*it);
				}
				break;
			}
			case PI_CHE:
			{
				//在垂直线上
				if ((*it)->logicPos[0] + goal->logicPos[0] == 10)
				{
					int minY, maxY;
					if ((*it)->logicPos[1] > 11 - goal->logicPos[1])
					{
						minY = 11 - goal->logicPos[1];
						maxY = (*it)->logicPos[1];
					}
					else
					{
						minY = (*it)->logicPos[1];
						maxY = 11 - goal->logicPos[1];
					}
					vector<Stc_Piece*>::iterator itr;
					for (itr = vecPiece.begin(); itr != vecPiece.end(); itr++)
					{
						if (*itr == *it || *itr == goal || !(*itr)->isAlive) continue;
						//将军方不需要转换位置
						if ((*itr)->player == attack)
						{
							if ((*itr)->logicPos[0] == (*it)->logicPos[0] && (*itr)->logicPos[1] > minY && (*itr)->logicPos[1] < maxY)
							{
								break;
							}
						}
						//被将军方要转换位置
						else
						{
							if (10 - (*itr)->logicPos[0] == (*it)->logicPos[0] && 11 - (*itr)->logicPos[1] > minY && 11 - (*itr)->logicPos[1] < maxY)
							{
								break;
							}
						}
					}
					if (itr == vecPiece.end())
					{
						canJiangPieces->push_back(*it);
					}
				}
				//在水平线上
				else if ((*it)->logicPos[1] + goal->logicPos[1] == 11)
				{
					int minX, maxX;
					if ((*it)->logicPos[0] > 10 - goal->logicPos[0])
					{
						minX = 10 - goal->logicPos[0];
						maxX = (*it)->logicPos[0];
					}
					else
					{
						minX = (*it)->logicPos[0];
						maxX = 10 - goal->logicPos[0];
					}
					vector<Stc_Piece*>::iterator itr;
					for (itr = vecPiece.begin(); itr != vecPiece.end(); itr++)
					{
						if (*itr == *it || *itr == goal || !(*itr)->isAlive) continue;
						//将军方不需要转换位置
						if ((*itr)->player == attack)
						{
							if ((*itr)->logicPos[1] == (*it)->logicPos[1] && (*itr)->logicPos[0] > minX && (*itr)->logicPos[0] < maxX)
							{
								break;
							}
						}
						//被将军方要转换位置
						else
						{
							if (11 - (*itr)->logicPos[1] == (*it)->logicPos[1] && 10 - (*itr)->logicPos[0] > minX && 10 - (*itr)->logicPos[0] < maxX)
							{
								break;
							}
						}
					}
					if (itr == vecPiece.end())
					{
						canJiangPieces->push_back(*it);
					}
				}
				break;
			}
			case PI_PAO:
			{
				int paoJia = 0;
				//在垂直线上
				if ((*it)->logicPos[0] + goal->logicPos[0] == 10)
				{
					int minY, maxY;
					if ((*it)->logicPos[1] > 11 - goal->logicPos[1])
					{
						minY = 11 - goal->logicPos[1];
						maxY = (*it)->logicPos[1];
					}
					else
					{
						minY = (*it)->logicPos[1];
						maxY = 11 - goal->logicPos[1];
					}
					vector<Stc_Piece*>::iterator itr;
					for (itr = vecPiece.begin(); itr != vecPiece.end(); itr++)
					{
						if (*itr == *it || *itr == goal || !(*itr)->isAlive) continue;
						//将军方不需要转换位置
						if ((*itr)->player == attack)
						{
							if ((*itr)->logicPos[0] == (*it)->logicPos[0] && (*itr)->logicPos[1] > minY && (*itr)->logicPos[1] < maxY)
							{
								paoJia++;
								if (paoJia > 1) break;
							}
						}
						//被将军方要转换位置
						else
						{
							if (10 - (*itr)->logicPos[0] == (*it)->logicPos[0] && 11 - (*itr)->logicPos[1] > minY && 11 - (*itr)->logicPos[1] < maxY)
							{
								paoJia++;
								if (paoJia > 1) break;
							}
						}
					}
					if (paoJia == 1)
					{
						canJiangPieces->push_back(*it);
					}
				}
				//在水平线上
				else if ((*it)->logicPos[1] + goal->logicPos[1] == 11)
				{
					int minX, maxX;
					if ((*it)->logicPos[0] > 10 - goal->logicPos[0])
					{
						minX = 10 - goal->logicPos[0];
						maxX = (*it)->logicPos[0];
					}
					else
					{
						minX = (*it)->logicPos[0];
						maxX = 10 - goal->logicPos[0];
					}
					vector<Stc_Piece*>::iterator itr;
					for (itr = vecPiece.begin(); itr != vecPiece.end(); itr++)
					{
						if (*itr == *it || *itr == goal || !(*itr)->isAlive) continue;
						//将军方不需要转换位置
						if ((*itr)->player == attack)
						{
							if ((*itr)->logicPos[1] == (*it)->logicPos[1] && (*itr)->logicPos[0] > minX && (*itr)->logicPos[0] < maxX)
							{
								paoJia++;
								if (paoJia > 1) break;
							}
						}
						//被将军方要转换位置
						else
						{
							if (11 - (*itr)->logicPos[1] == (*it)->logicPos[1] && 10 - (*itr)->logicPos[0] > minX && 10 - (*itr)->logicPos[0] < maxX)
							{
								paoJia++;
								if (paoJia > 1) break;
							}
						}
					}
					if (paoJia == 1)
					{
						canJiangPieces->push_back(*it);
					}
				}
				break;
			}
			case PI_ZHU:
			{
				//垂直方向
				if (goal->logicPos[0] + (*it)->logicPos[0] == 10 && (*it)->logicPos[1] + 1 == 11 - goal->logicPos[1])
				{
					canJiangPieces->push_back(*it);
				}
				//水平方向
				else if (goal->logicPos[1] + (*it)->logicPos[1] == 11)
				{
					if ((*it)->logicPos[0] - 1 == 10 - goal->logicPos[0] || (*it)->logicPos[0] + 1 == 10 - goal->logicPos[0])
					{
						canJiangPieces->push_back(*it);
					}
				}
				break;
			}
			default:
				break;
			}
		}
	}
	return canJiangPieces->size() > 0;
}

/*
* 判断游戏是否结束 
*/
bool RuleCenter::judgeWin(Em_Player attack)
{
	return false;
}
