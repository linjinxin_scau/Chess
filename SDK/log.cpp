#include "log.h"
#include <time.h>
#include <iostream>


/*
* 打印游戏信息
* @param msg 打印的数据
* @param filePath 打印文件的路径
* @param fileName 打印文件的文件名
*/
void logGameMsg(const char* msg, const char* filePath, const char* fileName)
{
	//取得系统时间
	time_t localTime;
	time(&localTime);
	tm* t = localtime(&localTime);

	//获得文件完整名
	std::string fullName = "";
	fullName.append(filePath != nullptr ? filePath : "D:/");
	if (fullName.at(fullName.size() - 1) != '/')
	{
		fullName.push_back('/');
	}
	char tStr[256];
	sprintf(tStr, "%s_game_%d%02d%02d.txt", fileName != nullptr ? fileName : "log", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday);
	fullName.append(tStr);

	//创建文件-写入文件-关闭文件
	FILE* file = fopen(fullName.c_str(), "a+");

	if (file == nullptr)
	{
		return;
	}

	fprintf(file, "[%02d:%02d:%02d] %s\n", t->tm_hour, t->tm_min, t->tm_sec, msg);

	fclose(file);
}

/*
* 打印错误信息
* @param msg 打印的数据
* @param filePath 打印文件的路径
* @param fileName 打印文件的文件名
*/
void logErrorMsg(const char* msg, const char* filePath, const char* fileName)
{
	//取得系统时间
	time_t localTime;
	time(&localTime);
	tm* t = localtime(&localTime);

	//获得文件完整名
	std::string fullName = "";
	fullName.append(filePath != nullptr ? filePath : "D:/");
	if (fullName.at(fullName.size() - 1) != '/')
	{
		fullName.push_back('/');
	}
	char tStr[256];
	sprintf(tStr, "%s_error_%d%02d%02d.txt", fileName != nullptr ? fileName : "log", t->tm_year + 1900, t->tm_mon + 1, t->tm_mday);
	fullName.append(tStr);

	//创建文件-写入文件-关闭文件
	FILE* file = fopen(fullName.c_str(), "a+");

	if (file == nullptr)
	{
		return;
	}

	fprintf(file, "[%02d:%02d:%02d] %s\n", t->tm_hour, t->tm_min, t->tm_sec, msg);

	fclose(file);
}
