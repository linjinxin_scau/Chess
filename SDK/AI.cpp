#include "AI.h"
#include <time.h>
#include <stdlib.h>

Stc_Piece* AI::goStep(int &runInx)
{
	time_t t;
	time(&t);
	tm *tm= localtime(&t);
	srand(tm->tm_sec);

	int begin = rand() % m_ruleCenter->vecPiece.size();
	int i = begin;
	int j;
	for (; i < m_ruleCenter->vecPiece.size(); i++)
	{
		if (m_ruleCenter->vecPiece[i]->player != m_ruleCenter->myCamp && m_ruleCenter->vecPiece[i]->isAlive)
		{
			m_ruleCenter->findCallFallPosition(m_ruleCenter->vecPiece[i]);

			int tempX = m_ruleCenter->vecPiece[i]->logicPos[0];
			int tempY = m_ruleCenter->vecPiece[i]->logicPos[1];
			
			j = 0;
			for (; j < MAX_FALL_COUNT; j++)
			{
				if (m_ruleCenter->vecPiece[i]->canFallPos[j][0] == 255)
				{
					break;
				}
				//临时更新位置以判断将军
				m_ruleCenter->vecPiece[i]->logicPos[0] = m_ruleCenter->vecPiece[i]->canFallPos[j][0];
				m_ruleCenter->vecPiece[i]->logicPos[1] = m_ruleCenter->vecPiece[i]->canFallPos[j][1];

				if (!m_ruleCenter->canJiangJu(m_ruleCenter->myCamp))
				{
					m_ruleCenter->vecPiece[i]->logicPos[0] = tempX;
					m_ruleCenter->vecPiece[i]->logicPos[1] = tempY;
					break;
				}

				printf("此步棋无法拯救将军，不能走！\n");
				m_ruleCenter->vecPiece[i]->logicPos[0] = tempX;
				m_ruleCenter->vecPiece[i]->logicPos[1] = tempY;
			}
			if (j < MAX_FALL_COUNT)
			{
				break;
			}
		}
	}

	if (i < m_ruleCenter->vecPiece.size())
	{
		runInx = j;
		return m_ruleCenter->vecPiece[i];
	}

	for (i = 0; i < begin; i++)
	{
		if (m_ruleCenter->vecPiece[i]->player != m_ruleCenter->myCamp && m_ruleCenter->vecPiece[i]->isAlive)
		{
			m_ruleCenter->findCallFallPosition(m_ruleCenter->vecPiece[i]);

			int tempX = m_ruleCenter->vecPiece[i]->logicPos[0];
			int tempY = m_ruleCenter->vecPiece[i]->logicPos[1];

			j = 0;
			for (; j < MAX_FALL_COUNT; j++)
			{
				if (m_ruleCenter->vecPiece[i]->canFallPos[j][0] == 255)
				{
					break;
				}
				//临时更新位置以判断将军
				m_ruleCenter->vecPiece[i]->logicPos[0] = m_ruleCenter->vecPiece[i]->canFallPos[j][0];
				m_ruleCenter->vecPiece[i]->logicPos[1] = m_ruleCenter->vecPiece[i]->canFallPos[j][1];

				if (!m_ruleCenter->canJiangJu(m_ruleCenter->myCamp))
				{
					m_ruleCenter->vecPiece[i]->logicPos[0] = tempX;
					m_ruleCenter->vecPiece[i]->logicPos[1] = tempY;
					break;
				}

				printf("此步棋无法拯救将军，不能走！\n");
				m_ruleCenter->vecPiece[i]->logicPos[0] = tempX;
				m_ruleCenter->vecPiece[i]->logicPos[1] = tempY;
			}
			if (j < MAX_FALL_COUNT)
			{
				break;
			}
		}
	}

	if (i < begin)
	{
		runInx = j;
		return m_ruleCenter->vecPiece[i];
	}

	return nullptr;
	//vector<Stc_Piece*>::iterator it = m_ruleCenter->vecPiece.begin();
	//for (; it != m_ruleCenter->vecPiece.end(); it++)
	//{
	//	if ((*it)->player != m_ruleCenter->myCamp && (*it)->isAlive)
	//	{
	//		m_ruleCenter->findCallFallPosition((*it));

	//		int tempX = (*it)->logicPos[0];
	//		int tempY = (*it)->logicPos[1];
	//		//临时更新位置以判断将军
	//		(*it)->logicPos[0] = (*it)->canFallPos[0][0];
	//		(*it)->logicPos[1] = (*it)->canFallPos[0][1];

	//		if (m_ruleCenter->canJiangJu(m_ruleCenter->myCamp))
	//		{
	//			printf("此步棋无法拯救将军，不能走！\n");
	//			(*it)->logicPos[0] = tempX;
	//			(*it)->logicPos[1] = tempY;
	//			continue;
	//		}

	//		(*it)->logicPos[0] = tempX;
	//		(*it)->logicPos[1] = tempY;

	//		if ((*it)->canFallPos[0][0] != 255)
	//		{
	//			break;
	//		}
	//	}
	//}

	//if (it == m_ruleCenter->vecPiece.end())
	//{
	//	return nullptr;
	//}
	//else
	//{
	//	runInx = 0;
	//	return *it;
	//}
}

int v[8] = { 0,0,5,5,10,30,20,8 };

int AI::calculateValue()
{
	int value = 0;

	vector<Stc_Piece*>::iterator it;
	//遍历所有棋子
	for (it = m_ruleCenter->vecPiece.begin(); it != m_ruleCenter->vecPiece.end(); it++)
	{
		if (!(*it)->isAlive)
		{
			continue;
		}
		value += v[(*it)->type] * ((*it)->player == m_ruleCenter->myCamp ? -1 : 1);
	}

	return value;
}

void AI::goStep(Stc_Piece * &piece, int & runInx)
{
	int i, j, tempX, tempY, value, maxValue;
	vector<Stc_Piece*>::iterator it;

	maxValue = -65535;

	//遍历所有棋子
	for (it = m_ruleCenter->vecPiece.begin(); it != m_ruleCenter->vecPiece.end(); it++)
	{
		if ((*it)->player != m_ruleCenter->myCamp && (*it)->isAlive)
		{
			m_ruleCenter->findCallFallPosition(*it);

			tempX = (*it)->logicPos[0];
			tempY = (*it)->logicPos[1];

			//遍历棋子所有能走的位置
			for (j = 0; j < MAX_FALL_COUNT; j++)
			{
				if ((*it)->canFallPos[j][0] == 255)
				{
					break;
				}

				//临时更新位置
				(*it)->logicPos[0] = (*it)->canFallPos[j][0];
				(*it)->logicPos[1] = (*it)->canFallPos[j][1];

				if (m_ruleCenter->canJiangJu(m_ruleCenter->myCamp))
				{
					printf("此步棋无法拯救将军，不能走！\n");
					continue;
				}

				value = calculateValue();
				if (value > maxValue)
				{
					maxValue = value;
					piece = *it;
					runInx = j;
				}
			}

			(*it)->logicPos[0] = tempX;
			(*it)->logicPos[1] = tempY;
		}
	}
}

void AI::setRuleCenter(RuleCenter * rc)
{
	this->m_ruleCenter = rc;
}
