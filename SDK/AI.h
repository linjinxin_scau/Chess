#ifndef __AI_H__
#define __AI_H__

#include "GameData.h"
#include "RuleCenter.h"

class AI
{
public:
	Stc_Piece* goStep(int &runInx);
	void goStep(Stc_Piece* &piece, int& runInx);
	int calculateValue();

	void setRuleCenter(RuleCenter* rc);

private:
	RuleCenter* m_ruleCenter;

};
#endif // !__AI_H__

