#ifndef __RULECENTER_H__
#define __RULECENTER_H__

#include "GameData.h"
#include <vector>
#include <map>

using namespace std;

class RuleCenter
{
public:
	RuleCenter();
	~RuleCenter();

	void findCallFallPosition(Stc_Piece* piece);
	bool canJiangJu(Em_Player attack);
	bool judgeWin(Em_Player attack);

public:
	Em_Player myCamp;	//我方阵营
	vector<Stc_Piece*> vecPiece;	//棋子数据
	vector<Stc_Piece*> mapCanJiangPiece[2];	//能将军的棋子
};

#endif // !__RULECENTER_H__

