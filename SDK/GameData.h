#ifndef __GAMEDATA_H__
#define __GAMEDATA_H__
/**
* 定义游戏数据
* 描述：客户端和服务端共同用到的纯数据、消息包
* 作者：林锦新
* 日期：2016年2月26日
*/
#include <iostream>

#define MAX_FALL_COUNT 18	//最大落子位置数

//棋子枚举
enum Em_Piece
{
	PI_INVALID,	//不合法
	PI_JIANG,	//将
	PI_SHI,		//士
	PI_XIANG,	//象
	PI_MA,		//马
	PI_CHE,		//车
	PI_PAO,		//炮
	PI_ZHU,		//卒
};

//对战双方
enum Em_Player
{
	PLAYER_RED=0,		//红方
	PLAYER_BLACK		//黑方
};

//棋子数据类
struct Stc_Piece
{
	Em_Piece type;	//棋子类型
	Em_Player player;//所属方
	bool isAlive;	//是否还存活
	unsigned char logicPos[2];//逻辑位置
	unsigned char canFallPos[MAX_FALL_COUNT][2];//能落子的位置
		
	void init()
	{
		type = PI_INVALID;
		player = PLAYER_RED;
		isAlive = true;
		memset(logicPos, 255, sizeof(logicPos));
		memset(canFallPos, 255, sizeof(canFallPos));
	}
};
#endif // !__GAMEDATA_H__

