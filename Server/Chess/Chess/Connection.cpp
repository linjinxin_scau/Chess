#include "Connection.h"
#include "rapidjson.h"

using namespace rapidjson;

Connection::Connection()
{
	try
	{
		m_server.set_access_channels(websocketpp::log::alevel::all);
		m_server.clear_access_channels(websocketpp::log::alevel::frame_payload);

		m_server.init_asio();

		m_server.set_message_handler(websocketpp::lib::bind(&Connection::onMessage, &m_server, ::_1, ::_2));

		m_server.listen(9002);

		m_server.start_accept();

		m_server.run();
	}
	catch (websocketpp::exception const &e)
	{
		cout << e.what() << endl;
	}
	catch (...)
	{
		cout << "other exception" << endl;
	}
}

Connection::~Connection()
{
}

/*
* 收到客户端消息
*/
void Connection::onMessage(server * s, websocketpp::connection_hdl hdl, message_ptr msg)
{
	cout << msg->get_payload() << endl;
	if (msg->get_payload() == "stop-listening")
	{
		s->stop_listening();
		return;
	}
}
