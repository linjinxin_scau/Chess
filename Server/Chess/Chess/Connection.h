#pragma once
#include <websocketpp/config/asio_no_tls.hpp>
#include <websocketpp/server.hpp>
#include <iostream>

using namespace std;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

typedef websocketpp::server<websocketpp::config::asio> server;
typedef server::message_ptr message_ptr;

class Connection
{
public:
	Connection();
	~Connection();

private:
	void onMessage(server* s, websocketpp::connection_hdl hdl, message_ptr msg);

private:
	server m_server;
};

