#ifndef __ABOUTSCENE_H__
#define __ABOUTSCENE_H__
/**
*
* 关于场景
* 描述：显示游戏创作相关信息
* 作者：林锦新
* 日期：2016年2月25日
*/
#include "cocos2d.h"
#include "SceneManager.h"

USING_NS_CC;

class AboutScene :public Layer
{
public:
	virtual bool init();
	static AboutScene* create(SceneManager* manager);
	static Scene* createScene(SceneManager* manager);

private:
	SceneManager* m_manager;
};
#endif // !__ABOUTSCENE_H__


