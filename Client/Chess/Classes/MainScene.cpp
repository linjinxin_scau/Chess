#include "base.h"
#include "../../../SDK/log.h"
#include "MainScene.h"
#include "cocostudio/CocoStudio.h"

/*
* 初始化层
*/
bool MainScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();

	auto node = CSLoader::createNode("MainScene.csb");

	if (node == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("初始化主场景根结点失败！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		this->addChild(node);

		initUI(node);

		initModeLayer(node);

		initLevelLayer(node);

		initRecordLayer(node);
	}

	//添加全局触摸事件
	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(MainScene::onTouch,this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	
	return true;
}

/*
* 创建层
*/
MainScene * MainScene::create(SceneManager* manager)
{
	auto pRet = new MainScene();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		pRet->m_manager = manager;
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

/*
* 创建场景和层，并把层加入到场景中
* @return 返回场景
*/
Scene * MainScene::createScene(SceneManager* manager)
{
	Scene* scene = Scene::create();
	if (scene == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建主场景失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
		return nullptr;
	}

	Layer* layer = MainScene::create(manager);
	if (layer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建主菜单层失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
	}
	else
	{
		scene->addChild(layer);
	}

	return scene;
}

/*
* 初始化基本层UI
*/
void MainScene::initUI(Node * node)
{
	m_mainLayer = dynamic_cast<Node*>(node->getChildByName("layer_main"));
	if (m_mainLayer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("初始化基本层失败！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		return;
	}
	//开始按钮
	m_startBtn = dynamic_cast<Button*>(m_mainLayer->getChildByName("btn_start"));
	if (m_startBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("开始按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_startBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onMenuBtnTouch, this));
		m_startBtn->runAction(Sequence::create(MoveBy::create(0.0f, Vec3(400, 0, 0)), MoveBy::create(1.0f, Vec3(-500, 0, 0)), MoveBy::create(0.3f, Vec3(100, 0, 0)), NULL));
	}

	//继续按钮
	m_goOnBtn = dynamic_cast<Button*>(m_mainLayer->getChildByName("btn_goon"));
	if (m_goOnBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("继续按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_goOnBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onMenuBtnTouch, this));
		m_goOnBtn->runAction(Sequence::create(MoveBy::create(0.0f, Vec3(-400, 0, 0)), MoveBy::create(1.0f, Vec3(500, 0, 0)), MoveBy::create(0.3f, Vec3(-100, 0, 0)), NULL));
	}

	//设置按钮
	m_setBtn = dynamic_cast<Button*>(m_mainLayer->getChildByName("btn_set"));
	if (m_setBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("设置按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_setBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onMenuBtnTouch, this));
		m_setBtn->runAction(Sequence::create(MoveBy::create(0.0f, Vec3(400, 0, 0)), MoveBy::create(1.0f, Vec3(-500, 0, 0)), MoveBy::create(0.3f, Vec3(100, 0, 0)), NULL));
	}

	//帮助按钮
	m_helpBtn = dynamic_cast<Button*>(m_mainLayer->getChildByName("btn_help"));
	if (m_helpBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("帮助按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_helpBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onMenuBtnTouch, this));
		m_helpBtn->runAction(Sequence::create(MoveBy::create(0.0f, Vec3(-400, 0, 0)), MoveBy::create(1.0f, Vec3(500, 0, 0)), MoveBy::create(0.3f, Vec3(-100, 0, 0)), NULL));
	}

	//关于按钮
	m_aboutBtn = dynamic_cast<Button*>(m_mainLayer->getChildByName("btn_about"));
	if (m_aboutBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("关于按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_aboutBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onMenuBtnTouch, this));
		m_aboutBtn->runAction(Sequence::create(MoveBy::create(0.0f, Vec3(400, 0, 0)), MoveBy::create(1.0f, Vec3(-500, 0, 0)), MoveBy::create(0.3f, Vec3(100, 0, 0)), NULL));
	}

	//退出按钮
	m_exitBtn = dynamic_cast<Button*>(m_mainLayer->getChildByName("btn_exit"));
	if (m_exitBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("退出按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_exitBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onMenuBtnTouch, this));
		m_exitBtn->runAction(Sequence::create(MoveBy::create(0.0f, Vec3(-400, 0, 0)), MoveBy::create(1.0f, Vec3(500, 0, 0)), MoveBy::create(0.3f, Vec3(-100, 0, 0)), NULL));
	}

	//logo
	m_logoSp = dynamic_cast<Sprite*>(m_mainLayer->getChildByName("sp_logo"));
	if (m_logoSp == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("logo精灵为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_logoSp->runAction(Sequence::create(FadeOut::create(0.0f), FadeIn::create(1.3f), NULL));
	}

	//背景
	m_bgSp = dynamic_cast<Sprite*>(m_mainLayer->getChildByName("sp_bg"));
	m_bgSp2 = dynamic_cast<Sprite*>(m_mainLayer->getChildByName("sp_bg2"));
	if (m_bgSp == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("背景精灵1为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else if (m_bgSp2 == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("背景精灵2为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		schedule(schedule_selector(MainScene::replaceBg), 5.0f);
	}
}

/*
* 初始化层：选择对战模式
*/
void MainScene::initModeLayer(Node * node)
{
	m_selectModeLayer = dynamic_cast<Node*>(node->getChildByName("layer_mode"));
	if (m_selectModeLayer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("选择模式层为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		return;
	}

	//默认不显示该层
	m_selectModeLayer->setVisible(false);

	//人机对战按钮
	m_machineBtn = dynamic_cast<Button*>(m_selectModeLayer->getChildByName("btn_machine"));
	if (m_machineBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("人机对战按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_machineBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onModeBtnTouch, this));
	}

	//网络对战按钮
	m_netBtn = dynamic_cast<Button*>(m_selectModeLayer->getChildByName("btn_net"));
	if (m_netBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("网络对战按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_netBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onModeBtnTouch, this));
	}

	//残局模式按钮
	m_messBtn = dynamic_cast<Button*>(node->getChildByName("btn_mess"));
	if (m_messBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("残局模式按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_messBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onModeBtnTouch, this));
	}
}

/*
* 初始化层：选择人机对战难度
*/
void MainScene::initLevelLayer(Node * node)
{
	m_selectLevelLayer = dynamic_cast<Node*>(node->getChildByName("layer_level"));
	if (m_selectLevelLayer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("选择人机难度层初始化失败！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		return;
	}

	//默认不显示该层
	m_selectLevelLayer->setVisible(false);
	
	//简单按钮
	m_easyBtn = dynamic_cast<Button*>(m_selectLevelLayer->getChildByName("btn_easy"));
	if (m_easyBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("初始化简单按钮失败！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_easyBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onLevelBtnTouch, this));
	}

	//简单按钮
	m_hardBtn = dynamic_cast<Button*>(m_selectLevelLayer->getChildByName("btn_hard"));
	if (m_hardBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("初始化困难按钮失败！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_hardBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onLevelBtnTouch, this));
	}

	//简单按钮
	m_masterBtn = dynamic_cast<Button*>(m_selectLevelLayer->getChildByName("btn_master"));
	if (m_masterBtn == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("初始化大师按钮失败！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		m_masterBtn->addTouchEventListener(CC_CALLBACK_2(MainScene::onLevelBtnTouch, this));
	}
}

/*
* 初始化层：选择存档
*/
void MainScene::initRecordLayer(Node * node)
{
}

/*
* 切换背景图片
* 计时器调度
*/
void MainScene::replaceBg(float delta)
{
	if (m_bgSp2->getOpacity() > 0)
	{
		if (random(0, 10) > 5)
		{
			m_bgSp->setFlipX(!m_bgSp->isFlippedX());
#ifdef OUTLOG
			logGameMsg("背景一反转！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		m_bgSp2->runAction(FadeOut::create(1.0f));
#ifdef OUTLOG
		logGameMsg("切换到背景一！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		if (random(0, 10) > 5)
		{
			m_bgSp2->setFlipX(!m_bgSp2->isFlippedX());
#ifdef OUTLOG
			logGameMsg("背景二反转！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		m_bgSp2->runAction(FadeIn::create(1.0f));
#ifdef OUTLOG
		logGameMsg("切换到背景二！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
}

/*
* 全局触摸事件
*/
bool MainScene::onTouch(Touch * t, Event * e)
{
	//隐藏游戏模式层	
	if (m_selectModeLayer)
	{
		m_selectModeLayer->setVisible(false);
	}
	//隐藏人机对战难度选择层
	if (m_selectLevelLayer)
	{
		m_selectLevelLayer->setVisible(false);
	}
	//启用菜单按钮
	m_startBtn->setTouchEnabled(true);
	m_goOnBtn->setTouchEnabled(true);
	m_setBtn->setTouchEnabled(true);
	m_aboutBtn->setTouchEnabled(true);
	m_helpBtn->setTouchEnabled(true);
	m_exitBtn->setTouchEnabled(true);
	return false;
}

/*
* 点击菜单按钮事件
*/
void MainScene::onMenuBtnTouch(Ref * pRef, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)
	{
		auto btn = (Button*)pRef;
		if (btn == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("触发触摸事件的不是按钮！", WRITEABLEPATH, "ui");
#endif // OUTLOG
			return;
		}
		if (m_manager == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("场景管理器为空！", WRITEABLEPATH, "scene");
#endif // OUTLOG
			return;
		}
		if (btn == m_startBtn)
		{
			if (m_selectLevelLayer != nullptr)
			{
				//显示游戏模式层
				m_selectModeLayer->setVisible(true);
				//屏弊菜单按钮
				m_startBtn->setTouchEnabled(false);
				m_goOnBtn->setTouchEnabled(false);
				m_setBtn->setTouchEnabled(false);
				m_aboutBtn->setTouchEnabled(false);
				m_helpBtn->setTouchEnabled(false);
				m_exitBtn->setTouchEnabled(false);
			}
		}
		else if (btn == m_goOnBtn)
		{

		}
		else if (btn == m_setBtn)
		{
			m_manager->gotoSetScene();
		}
		else if (btn == m_aboutBtn)
		{
			m_manager->gotoAboutScene();
		}
		else if (btn == m_helpBtn)
		{
			m_manager->gotoHelpScene();
		}
		else if (btn == m_exitBtn)
		{
			m_manager->exitGame();
		}
	}
}

/*
* 点击游戏模式按钮事件
*/
void MainScene::onModeBtnTouch(Ref * pRef, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)
	{
		auto btn = dynamic_cast<Button*>(pRef);
		if (btn == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("触发模式触摸事件的不是按钮!", WRITEABLEPATH, "ui");
#endif // OUTLOG
			return;
		}

		//隐藏显示游戏模式层
		m_selectModeLayer->setVisible(false);

		if (btn == m_machineBtn)
		{
			//显示人机对战难度选择层
			m_selectLevelLayer->setVisible(true);
		}
		else if (btn == m_netBtn)
		{
			//转到网络对战玩家匹配场景
			m_manager->gotoMatchScene();
		}
		else if (btn == m_messBtn)
		{

		}
	}
}

/*
* 点击人机玩法难度按钮事件
*/
void MainScene::onLevelBtnTouch(Ref * pRef, Widget::TouchEventType type)
{
	if (type == Widget::TouchEventType::ENDED)
	{
		auto btn = dynamic_cast<Button*>(pRef);
		if (btn == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("触发难度触摸事件的不是按钮!", WRITEABLEPATH, "ui");
#endif // OUTLOG
			return;
		}

		//隐藏人机对战难度选择层
		m_selectLevelLayer->setVisible(false);

		//转到游戏场景
		m_manager->gotoGameScene();
	}
}
