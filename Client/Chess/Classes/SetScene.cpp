#include "base.h"
#include "../../../SDK/log.h"
#include "SetScene.h"

/*
* 初始化层
*/
bool SetScene::init()
{
	if (!Layer::init())
	{
		return false;
	}
	return true;
}

/*
* 创建层
*/
SetScene * SetScene::create(SceneManager* manager)
{
	auto pRet = new SetScene();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		pRet->m_manager = manager;
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

/*
* 创建场景和层，并把层加入到场景中
* @return 返回场景
*/
Scene * SetScene::createScene(SceneManager* manager)
{
	Scene* scene = Scene::create();
	if (scene == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建设置场景失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
		return nullptr;
	}

	Layer* layer = SetScene::create(manager);
	if (layer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建设置层失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
	}
	else
	{
		scene->addChild(layer);
	}

	return scene;
}
