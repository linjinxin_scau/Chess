#ifndef __GAMESCENE_H__
#define __GAMESCENE_H__
/**
*
* 游戏场景
* 描述：真人对战和人机对战使用的游戏场景
* 作者：林锦新
* 日期：2016年2月25日
*/
#include "cocos2d.h"
#include "SceneManager.h"
#include "Piece.h"
#include <vector>
#include "ui/CocosGUI.h"
#include "../../../SDK/RuleCenter.h"
#include "../../../SDK/AI.h"

using namespace cocos2d::ui;
USING_NS_CC;
using namespace std;

//逻辑阶段
enum PHASE
{
	PH_WAIT_OTHER_GO,//等待对方落子
	PH_WAIT_SELECT,//等待选子
	PH_WAIT_GO,//等待落子
};

class GameScene :public Layer
{
public:
	~GameScene();
	static GameScene* create(SceneManager* manager);
	static Scene* createScene(SceneManager* manager);

	virtual bool init();

	void initPieces();
	void movePiece(int inx, Em_Player player);
	void goWithAI();

	bool onTouchBegan(Touch* t, Event*);

private:
	Vec2 transferPosition(bool isMe, int x, int y);
	void drawCanFallPoint();

public:
	SceneManager* m_manager;

	RuleCenter m_ruleCenter;
	AI	m_AI;
	PHASE m_phase;

	vector<Piece> m_vecPiece;
	Piece* m_selectPiece;
	Sprite* m_pieceBox;

	Sprite* m_bgSprite;
	Sprite* m_boardSprite;
	Button* m_regretBtn;
	Button* m_exitBtn;
	Text* m_jiangText;
	Text* m_warnText;
	Sprite* m_winSprite;
	Sprite* m_lostSprite;

	DrawNode* m_drawNode;
};

#endif // !__GAMESCENE_H__


