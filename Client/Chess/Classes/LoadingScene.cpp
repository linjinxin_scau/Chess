#include "base.h"
#include "../../../SDK/log.h"
#include "LoadingScene.h"
#include "cocostudio/CocoStudio.h"
#include "tinyxml2/tinyxml2.h"

/*
* 初始化层
*/
bool LoadingScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto node = CSLoader::createNode("LoadScene.csb");
	this->addChild(node);

	preLoadPic();

	return true;
}

/*
* 创建层
*/
LoadingScene * LoadingScene::create(SceneManager* manager)
{
	auto pRet = new LoadingScene();

	if (pRet && pRet->init())
	{
		pRet->m_Manager = manager;
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

/*
* 创建场景和层，并把层加入到场景中
* @return 返回场景
*/
Scene * LoadingScene::createScene(SceneManager* manager)
{
	Scene* scene = Scene::create();
	if (scene == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建加载场景失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
		return nullptr;
	}

	Layer* layer = LoadingScene::create(manager);
	if (layer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建加载层失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG		
	}
	else
	{
		scene->addChild(layer);
	}
	
	return scene;
}

/*
* 预加载图片
*/
void LoadingScene::preLoadPic()
{
	//加载xml文档
	tinyxml2::XMLDocument doc;
	doc.Parse(FileUtils::getInstance()->getStringFromFile(FileUtils::getInstance()->fullPathForFilename("config/preload.xml")).c_str());

	if (doc.Error())
	{
#ifdef OUTLOG
		logErrorMsg("加载xml文档失败！", WRITEABLEPATH, "preload");
#endif // OUTLOG
		return;
	}

	//得到xml文档根结点
	tinyxml2::XMLElement* root = doc.RootElement();

	//得到图片根结点
	auto picNode = root->FirstChildElement();

	if (strcmp(picNode->Name(), "pic") != 0)
	{
#ifdef OUTLOG
		logErrorMsg("预加载图片失败！", WRITEABLEPATH,"preload");
#endif // OUTLOG
		return;
	}

	//解析子结点
	std::string path,type;
	for (tinyxml2::XMLElement* e = picNode->FirstChildElement(); e != nullptr; e = e->NextSiblingElement())
	{
		path = e->Attribute("name");
		type = e->Attribute("type");
		if (path.size() == 0)
		{
#ifdef OUTLOG
			logErrorMsg("预加载图片路径为空！", WRITEABLEPATH, "preload");
#endif // OUTLOG
			continue;
		}
		if (type == "plist")
		{
			SpriteFrameCache::getInstance()->addSpriteFramesWithFile(path);
		}
		else if (type == "image")
		{
		}
		else
		{
#ifdef OUTLOG
			logErrorMsg("预加载图片类型未知！", WRITEABLEPATH, "preload");
#endif // OUTLOG
		}
	}

	preLoadMusic();
}

/*
* 预加载音乐
*/
void LoadingScene::preLoadMusic()
{
	this->schedule(schedule_selector(LoadingScene::loadFinish), 3.0f);
}

void LoadingScene::loadFinish(float delta)
{
	this->unschedule(schedule_selector(LoadingScene::loadFinish));

	if (m_Manager)
	{
		m_Manager->gotoMainScene();
	}
	else
	{
#ifdef OUTLOG
		logErrorMsg("场景管理器为空！", WRITEABLEPATH, "scene");
#endif // OUTLOG
	}
}
