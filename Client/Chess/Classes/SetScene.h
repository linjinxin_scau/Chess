#ifndef __SETSCENE_H__
#define __SETSCENE_H__
/**
*
* 设置场景
* 描述：设置游戏背景音乐、音效、背景等
* 作者：林锦新
* 日期：2016年2月19日
*/
#include "cocos2d.h"
#include "SceneManager.h"

USING_NS_CC;

class SetScene :public Layer
{
public:
	virtual bool init();
	static SetScene* create(SceneManager* manager);
	static Scene* createScene(SceneManager* manager);

private:
	SceneManager* m_manager;
};
#endif // !__SETSCENE_H__


