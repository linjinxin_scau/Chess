#include "base.h"
#include "../../../SDK/log.h"
#include "MatchScene.h"
#include "cocostudio/CocoStudio.h"

/*
* 初始化层
*/
bool MatchScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	return true;
}

/*
* 创建层
*/
MatchScene * MatchScene::create(SceneManager* manager)
{
	auto pRet = new MatchScene();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		pRet->m_manager = manager;
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

/*
* 创建场景和层，并把层加入到场景中
* @return 返回场景
*/
Scene * MatchScene::createScene(SceneManager* manager)
{
	Scene* scene = Scene::create();
	if (scene == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建匹配场景失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
		return nullptr;
	}

	Layer* layer = MatchScene::create(manager);
	if (layer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建匹配层失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
	}
	else
	{
		scene->addChild(layer);
	}

	return scene;
}
