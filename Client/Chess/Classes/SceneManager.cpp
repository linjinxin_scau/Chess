#include "SceneManager.h"
#include "LoadingScene.h"
#include "MainScene.h"
#include "SetScene.h"
#include "AboutScene.h"
#include "HelpScene.h"
#include "MatchScene.h"
#include "GameScene.h"

//静态成员变量要在cpp文件显式声明
SceneManager* SceneManager::m_ins;

/*
* 单例模式
*/
SceneManager * SceneManager::getInstance()
{
	if (m_ins == nullptr)
	{
		m_ins = new SceneManager();
	}
	return m_ins;
}

/*
* 初始化加载场景
*/
void SceneManager::initScene()
{
	auto scene = LoadingScene::createScene(this);
	if (scene == nullptr)
	{
		return;
	}
	Director::getInstance()->runWithScene(scene);
}

/*
* 转到主菜单场景
*/
void SceneManager::gotoMainScene()
{
	auto scene = MainScene::createScene(this);
	if (scene == nullptr)
	{
		return;
	}
	Director::getInstance()->replaceScene(scene);
}

/*
* 转到关于场景
*/
void SceneManager::gotoAboutScene()
{
	auto scene = AboutScene::createScene(this);
	if (scene == nullptr)
	{
		return;
	}
	auto transition = TransitionRotoZoom::create(0.5f, scene);
	Director::getInstance()->replaceScene(transition);
}

/*
* 转到设置场景
*/
void SceneManager::gotoSetScene()
{
	auto scene = SetScene::createScene(this);
	if (scene == nullptr)
	{
		return;
	}
	auto transition = TransitionJumpZoom::create(0.5f, scene);
	Director::getInstance()->replaceScene(transition);
}

/*
* 转到帮助场景
*/
void SceneManager::gotoHelpScene()
{
	auto scene = HelpScene::createScene(this);
	if (scene == nullptr)
	{
		return;
	}
	auto transition = TransitionFlipX::create(0.5f, scene);
	Director::getInstance()->replaceScene(transition);
}

/*
* 转到匹配玩家场景 
*/
void SceneManager::gotoMatchScene()
{
	auto scene = MatchScene::createScene(this);
	if (scene == nullptr)
	{
		return;
	}
	auto transition = TransitionCrossFade::create(0.5f, scene);
	Director::getInstance()->replaceScene(transition);
}

/*
* 转到游戏场景
*/
void SceneManager::gotoGameScene()
{
	auto scene = GameScene::createScene(this);
	if (scene == nullptr)
	{
		return;
	}
	auto transition = TransitionFade::create(0.5f, scene);
	Director::getInstance()->replaceScene(transition);
}

/*
* 退出游戏
*/
void SceneManager::exitGame()
{
	Director::getInstance()->end();
}
