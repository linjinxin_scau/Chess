#ifndef __HELPSCENE_H__
#define __HELPSCENE_H__
/**
*
* 帮助场景
* 描述：显示帮助信息,游戏玩法等
* 作者：林锦新
* 日期：2016年2月25日
*/
#include "cocos2d.h"
#include "SceneManager.h"

USING_NS_CC;

class HelpScene :public Layer
{
public:
	virtual bool init();
	static HelpScene* create(SceneManager* manager);
	static Scene* createScene(SceneManager* manager);

private:
	SceneManager* m_manager;
};
#endif // !__HELPSCENE_H__


