#ifndef __SCENEMANAGER_H__
#define __SCENEMANAGER_H__
/**
*
* 场景管理器
* 描述：统一管理游戏中的场景，场景创建、场景切换
* 作者：林锦新
* 日期：2017年2月19日
*/

class SceneManager
{
public:
	static SceneManager* getInstance();

	void initScene();
	void gotoMainScene();
	void gotoAboutScene();
	void gotoSetScene();
	void gotoHelpScene();
	void gotoMatchScene();
	void gotoGameScene();
	void exitGame();

private:
	static SceneManager* m_ins;
};
#endif // !__SCENEMANAGER_H__
