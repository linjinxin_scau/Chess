#include "Piece.h"

Stc_Piece* Piece::getData()
{
	return this->data;
}

Sprite * Piece::getSprite()
{
	return this->sprite;
}

void Piece::setSprite(Sprite * sp)
{
	this->sprite = sp;
}

void Piece::setData(Stc_Piece *data)
{
	this->data = data;
}

void Piece::setData(Em_Piece type, Em_Player player, bool isAlive, unsigned char pos[])
{
	this->data = new Stc_Piece();
	this->data->init();
	this->data->type = type;
	this->data->player = player;
	this->data->isAlive = isAlive;
	this->data->logicPos[0] = pos[0];
	this->data->logicPos[1] = pos[1];
}

/*
* ��������λ��
* @param x �߼�λ��x
* @param y �߼�λ��y
*/
void Piece::updatePos(int x, int y)
{
	this->data->logicPos[0] = x;
	this->data->logicPos[1] = y;
}
