#ifndef __PIECE_H__
#define __PIECE_H__
/**
* 棋子实体类
* 描述：客户端用于显示的一个棋子，包含数据和显示
* 作者：林锦新
* 日期：2017年2月26日
*/

#include "cocos2d.h"
#include "../../../SDK/GameData.h"

USING_NS_CC;

class Piece
{
public:
	Sprite* getSprite();
	void setSprite(Sprite* sp);
	Stc_Piece* getData();
	void setData(Stc_Piece*);
	void setData(Em_Piece type, Em_Player player, bool isAlive, unsigned char pos[]);

	void updatePos(int x, int y);

private:
	Sprite* sprite;
	Stc_Piece* data;

};
#endif // !__PIECE_H__

