#include "base.h"
#include "../../../SDK/log.h"
#include "HelpScene.h"
#include "cocostudio/CocoStudio.h"

/*
* 初始化层
*/
bool HelpScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto node = CSLoader::createNode("HelpScene.csb");
	this->addChild(node);

	return true;
}

/*
* 创建层
*/
HelpScene * HelpScene::create(SceneManager* manager)
{
	auto pRet = new HelpScene();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		pRet->m_manager = manager;
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

/*
* 创建场景和层，并把层加入到场景中
* @return 返回场景
*/
Scene * HelpScene::createScene(SceneManager* manager)
{
	Scene* scene = Scene::create();
	if (scene == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建帮助场景失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
		return nullptr;
	}

	Layer* layer = HelpScene::create(manager);
	if (layer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建帮助层失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
	}
	else
	{
		scene->addChild(layer);
	}

	return scene;
}
