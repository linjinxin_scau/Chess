#ifndef __MAINSCENE_H__
#define __MAINSCENE_H__
/**
*
* 主场景
* 描述：显示游戏主菜单
* 作者：林锦新
* 日期：2016年2月19日
*/
#include "cocos2d.h"
#include "SceneManager.h"
#include "ui/CocosGUI.h"

USING_NS_CC;
using namespace cocos2d::ui;

class MainScene :public Layer
{
public:
	virtual bool init();
	static MainScene* create(SceneManager* manager);
	static Scene* createScene(SceneManager* manager);

private:
	SceneManager* m_manager;

	Button* m_startBtn;
	Button* m_goOnBtn;
	Button* m_setBtn;
	Button* m_helpBtn;
	Button* m_aboutBtn;
	Button* m_exitBtn;
	Sprite* m_logoSp;
	Sprite* m_bgSp;
	Sprite* m_bgSp2;

	Node* m_mainLayer;
	Node* m_selectModeLayer;
	Node* m_selectLevelLayer;
	Node* m_selectRecordLayer;

	//选择模式 单机 网络 残局
	Button* m_machineBtn;
	Button* m_netBtn;
	Button* m_messBtn;

	//选择人机难度 简单 困难 大师
	Button* m_easyBtn;
	Button* m_hardBtn;
	Button* m_masterBtn;

private:
	void initUI(Node* node);
	void initModeLayer(Node* node);
	void initLevelLayer(Node* node);
	void initRecordLayer(Node* node);

	void replaceBg(float delta);

	bool onTouch(Touch* t, Event* e);
	void onMenuBtnTouch(Ref* pRef, Widget::TouchEventType type);
	void onModeBtnTouch(Ref* pRef, Widget::TouchEventType type);
	void onLevelBtnTouch(Ref* pRef, Widget::TouchEventType type);
};
#endif // !__MAINSCENE_H__



