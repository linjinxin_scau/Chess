#include "base.h"
#include "../../../SDK/log.h"
#include "AboutScene.h"
#include "cocostudio/CocoStudio.h"
#include "ui/CocosGUI.h"

using namespace cocos2d::ui;

/*
* 初始化层
*/
bool AboutScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto node = CSLoader::createNode("AboutScene.csb");
	this->addChild(node);

	auto rt = RichText::create();
	
	return true;
}

/*
* 创建层
*/
AboutScene * AboutScene::create(SceneManager* manager)
{
	auto pRet = new AboutScene();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		pRet->m_manager = manager;
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

/*
* 创建场景和层，并把层加入到场景中
* @return 返回场景
*/
Scene * AboutScene::createScene(SceneManager* manager)
{
	Scene* scene = Scene::create();
	if (scene == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建关于场景失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
		return nullptr;
	}

	Layer* layer = AboutScene::create(manager);
	if (layer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建关于层失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
	}
	else
	{
		scene->addChild(layer);
	}

	return scene;
}
