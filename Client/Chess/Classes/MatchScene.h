#ifndef __MATCHSCENE_H__
#define __MATCHSCENE_H__
/**
*
* 匹配场景
* 描述：真人对战模式匹配其它网络玩家
* 作者：林锦新
* 日期：2016年2月25日
*/
#include "cocos2d.h"
#include "SceneManager.h"

USING_NS_CC;

class MatchScene :public Layer
{
public:
	virtual bool init();
	static MatchScene* create(SceneManager* manager);
	static Scene* createScene(SceneManager* manager);

private:
	SceneManager* m_manager;
};
#endif // !__MATCHSCENE_H__


