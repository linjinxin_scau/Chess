#ifndef __LOADINGSCENE_H__
#define __LOADINGSCENE_H__
/**
*
* 加载场景
* 描述：预加载游戏中的资源，播放加载动画
* 作者：林锦新
* 日期：2016年2月19日
*/
#include "cocos2d.h"
#include "SceneManager.h"

USING_NS_CC;

class LoadingScene :public Layer
{
public:
	virtual bool init();
	static LoadingScene* create(SceneManager* manager);
	static Scene* createScene(SceneManager* manager);

private:
	SceneManager* m_Manager;

public:
	void preLoadPic();
	void preLoadMusic();
	void loadFinish(float delta);
	void loadPicFinish();
	void loadMusicFinish();
};

#endif // !__LOADINGSCENE_H__

