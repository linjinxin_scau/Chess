#include "base.h"
#include "../../../SDK/log.h"
#include "GameScene.h"
#include "cocostudio/CocoStudio.h"
#include "GlobalSet.h"

GameScene::~GameScene()
{
	if (m_manager)
	{
		delete m_manager;
		m_manager = nullptr;
	}
	
	if (m_selectPiece)
	{
		//delete m_selectPiece;
		//m_selectPiece = nullptr;
	}
}

/*
* 创建层
*/
GameScene * GameScene::create(SceneManager* manager)
{
	auto pRet = new GameScene();
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		pRet->m_manager = manager;
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = nullptr;
		return nullptr;
	}
}

/*
* 创建场景和层，并把层加入到场景中
* @return 返回场景
*/
Scene * GameScene::createScene(SceneManager* manager)
{
	Scene* scene = Scene::create();
	if (scene == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建游戏场景失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
		return nullptr;
	}

	Layer* layer = GameScene::create(manager);
	if (layer == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("创建游戏层失败！", WRITEABLEPATH, "scene");
#endif // OUTLOG
	}
	else
	{
		scene->addChild(layer);
	}

	return scene;
}

/*
* 初始化层
*/
bool GameScene::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto node = CSLoader::createNode("GameScene.csb");
	if (node == nullptr)
	{
#ifdef OUTLOG
		logErrorMsg("初始化游戏场景根结点失败！", WRITEABLEPATH, "ui");
#endif // OUTLOG
	}
	else
	{
		this->addChild(node);

		//背景
		m_bgSprite = dynamic_cast<Sprite*>(node->getChildByName("sp_bg"));
		if (m_bgSprite == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("游戏场景背景精灵为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		else
		{
			auto texture=TextureCache::getInstance()->getTextureForKey("style_2/bg.png");
			if (texture == nullptr)
			{
				TextureCache::getInstance()->addImageAsync("style_1/bg.png", [this](Texture2D* t)
				{
					m_bgSprite->setTexture(t);
				}
				);
			}
			else
			{
				m_bgSprite->setTexture(texture);
			}
		}

		//棋盘
		m_boardSprite = dynamic_cast<Sprite*>(node->getChildByName("sp_board"));
		if (m_boardSprite == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("游戏场景棋盘精灵为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		else
		{
			auto texture = TextureCache::getInstance()->getTextureForKey("style_1/board.png");
			if (texture == nullptr)
			{
				TextureCache::getInstance()->addImageAsync("style_1/board.png", [this](Texture2D* t)
				{
					m_boardSprite->setTexture(t);
				}
				);
			}
		}

		//悔棋按钮
		m_regretBtn = dynamic_cast<Button*>(node->getChildByName("btn_regret"));
		if (m_regretBtn == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("悔棋按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		else
		{
			m_regretBtn->addTouchEventListener([](Ref* pRef, Widget::TouchEventType type)
			{
				if (type == Widget::TouchEventType::BEGAN)
				{
					log("regret");
				}
			});
		}

		//退出按钮
		m_exitBtn = dynamic_cast<Button*>(node->getChildByName("btn_exit"));
		if (m_exitBtn == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("退出按钮为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		else
		{
			m_exitBtn->addTouchEventListener([this](Ref* pRef, Widget::TouchEventType type)
			{
				if (type == Widget::TouchEventType::BEGAN)
				{
					log("exit");
					m_manager->gotoMainScene();
				}
			});
		}

		//将军文本
		m_jiangText = dynamic_cast<Text*>(node->getChildByName("text_jiang"));
		if (m_jiangText == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("将军文本为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		else
		{
			m_jiangText->removeFromParent();
			this->addChild(m_jiangText);
			m_jiangText->runAction(FadeOut::create(0));
		}

		//警告文本
		m_warnText = dynamic_cast<Text*>(node->getChildByName("text_warn"));
		if (m_warnText == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("警告文本为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		else
		{
			m_warnText->removeFromParent();
			this->addChild(m_warnText);
			m_warnText->runAction(FadeOut::create(0));
		}

		//胜利精灵
		m_winSprite = dynamic_cast<Sprite*>(node->getChildByName("sp_win"));
		if (m_winSprite == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("胜利精灵为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		else
		{
			m_winSprite->removeFromParent();
		}

		//失败精灵
		m_lostSprite = dynamic_cast<Sprite*>(node->getChildByName("sp_lost"));
		if (m_lostSprite == nullptr)
		{
#ifdef OUTLOG
			logErrorMsg("失败精灵为空！", WRITEABLEPATH, "ui");
#endif // OUTLOG
		}
		else
		{
			m_lostSprite->removeFromParent();
		}
	}

	//初始化棋子并显示
	initPieces();

	//添加绘制组件
	m_drawNode = DrawNode::create();
	this->addChild(m_drawNode);

	//添加胜利失败精灵
	if(m_winSprite)
	{
		this->addChild(m_winSprite);
	}
	if (m_lostSprite)
	{
		this->addChild(m_lostSprite);
	}

	//添加触摸事件
	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(GameScene::onTouchBegan, this);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

	return true;
}

/*
* 初始化棋子
*/
void GameScene::initPieces()
{
	//初始化规则中心
	m_ruleCenter.myCamp = PLAYER_RED;
	m_ruleCenter.vecPiece.clear();

	//初始化游戏阶段
	m_phase = m_ruleCenter.myCamp == PLAYER_RED ? PH_WAIT_SELECT : PH_WAIT_OTHER_GO;

	//初始化棋子
	m_vecPiece.clear();

	auto spfc = SpriteFrameCache::getInstance();

	//红帅
	auto spf = spfc->getSpriteFrameByName("style_2/r_j.png");
	auto sp = Sprite::createWithSpriteFrame(spf);
	auto piece = Piece();
	piece.setSprite(sp);
	unsigned char pos[2] = { 5,1 };
	piece.setData(PI_JIANG, PLAYER_RED, true, pos);
	m_vecPiece.push_back(piece);
	m_ruleCenter.vecPiece.push_back(piece.getData());

	//红仕
	spf = spfc->getSpriteFrameByName("style_1/r_s.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 4 : 6;
		pos[1] = 1;
		piece.setData(PI_SHI, PLAYER_RED, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//红相
	spf = spfc->getSpriteFrameByName("style_1/r_x.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 3 : 7;
		pos[1] = 1;
		piece.setData(PI_XIANG, PLAYER_RED, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//红马
	spf = spfc->getSpriteFrameByName("style_1/r_m.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 2 : 8;
		pos[1] = 1;
		piece.setData(PI_MA, PLAYER_RED, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//红车
	spf = spfc->getSpriteFrameByName("style_1/r_c.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 1 : 9;
		pos[1] = 1;
		piece.setData(PI_CHE, PLAYER_RED, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//红炮
	spf = spfc->getSpriteFrameByName("style_1/r_p.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 2 : 8;
		pos[1] = 3;
		piece.setData(PI_PAO, PLAYER_RED, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//红兵
	spf = spfc->getSpriteFrameByName("style_1/r_z.png");
	for (int i = 0; i < 5; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = 1 + 2 * i;
		pos[1] = 4;
		piece.setData(PI_ZHU, PLAYER_RED, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//黑将
	spf = spfc->getSpriteFrameByName("style_1/b_j.png");
	sp = Sprite::createWithSpriteFrame(spf);
	piece = Piece();
	piece.setSprite(sp);
	pos[0] = 5; pos[1] = 1;
	piece.setData(PI_JIANG, PLAYER_BLACK, true, pos);
	m_vecPiece.push_back(piece);
	m_ruleCenter.vecPiece.push_back(piece.getData());

	//黑士
	spf = spfc->getSpriteFrameByName("style_1/b_s.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 4 : 6;
		pos[1] = 1;
		piece.setData(PI_SHI, PLAYER_BLACK, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//黑象
	spf = spfc->getSpriteFrameByName("style_1/b_x.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 3 : 7;
		pos[1] = 1;
		piece.setData(PI_XIANG, PLAYER_BLACK, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//黑马
	spf = spfc->getSpriteFrameByName("style_1/b_m.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 2 : 8;
		pos[1] = 1;
		piece.setData(PI_MA, PLAYER_BLACK, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//黑车
	spf = spfc->getSpriteFrameByName("style_1/b_c.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 1 : 9;
		pos[1] = 1;
		piece.setData(PI_CHE, PLAYER_BLACK, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//黑炮
	spf = spfc->getSpriteFrameByName("style_1/b_p.png");
	for (int i = 0; i < 2; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = i == 0 ? 2 : 8;
		pos[1] = 3;
		piece.setData(PI_PAO, PLAYER_BLACK, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//黑卒
	spf = spfc->getSpriteFrameByName("style_1/b_z.png");
	for (int i = 0; i < 5; i++)
	{
		sp = Sprite::createWithSpriteFrame(spf);
		piece = Piece();
		piece.setSprite(sp);
		pos[0] = 1 + 2 * i;
		pos[1] = 4;
		piece.setData(PI_ZHU, PLAYER_BLACK, true, pos);
		m_vecPiece.push_back(piece);
		m_ruleCenter.vecPiece.push_back(piece.getData());
	}

	//显示棋子
	for (vector<Piece>::iterator it = m_vecPiece.begin(); it != m_vecPiece.end(); it++)
	{
		auto sp = it->getSprite();
		this->addChild(sp);
		bool isMe = it->getData()->player == m_ruleCenter.myCamp;
		sp->setPosition(transferPosition(isMe, it->getData()->logicPos[0], it->getData()->logicPos[1]));
	}

	//选中框
	spf = spfc->getSpriteFrameByName("style_1/r_box.png");
	m_pieceBox = Sprite::createWithSpriteFrame(spf);
	m_pieceBox->setVisible(false);
	this->addChild(m_pieceBox);

	//初始化AI
	m_AI.setRuleCenter(&m_ruleCenter);
}

/*
* 移动棋子并判断是否可吃可将军
* @param inx 可走位置的下标
* @param player 移动的棋子所属阵营
*/
void GameScene::movePiece(int inx, Em_Player player)
{
	//绘制移动路径
	m_drawNode->clear();
	Vec2 pos;
	if (player == m_ruleCenter.myCamp)
	{
		pos = transferPosition(true, m_selectPiece->getData()->logicPos[0], m_selectPiece->getData()->logicPos[1]);
	}
	else
	{
		pos = transferPosition(true, 10 - m_selectPiece->getData()->logicPos[0], 11 - m_selectPiece->getData()->logicPos[1]);
	}
	m_drawNode->drawCircle(pos, piece_radius, 0, 60, false, Color4F(0, 1.0, 0, 1.0f));
	
	//移动棋子
	m_selectPiece->updatePos(m_selectPiece->getData()->canFallPos[inx][0], m_selectPiece->getData()->canFallPos[inx][1]);
	m_selectPiece->getSprite()->setPosition(transferPosition(player == m_ruleCenter.myCamp, m_selectPiece->getData()->logicPos[0], m_selectPiece->getData()->logicPos[1]));
	m_selectPiece->getSprite()->runAction(RepeatForever::create(Blink::create(0.8f, 1)));	//闪烁
	
	//判断是否可吃棋子
	for (vector<Piece>::iterator it = m_vecPiece.begin(); it != m_vecPiece.end(); it++)
	{
		if (it->getData()->player == player)
		{
			continue;
		}
		if (it->getData()->isAlive == false)
		{
			continue;
		}
		if (it->getData()->logicPos[0] + m_selectPiece->getData()->logicPos[0] == 10 && it->getData()->logicPos[1] + m_selectPiece->getData()->logicPos[1] == 11)
		{
			it->getData()->isAlive = false;
			it->getSprite()->setVisible(false);
			break;
		}
	}
	//判断能否将军对方
	if (m_ruleCenter.canJiangJu(player))
	{
		m_jiangText->runAction(Sequence::create(FadeIn::create(0.3f), FadeOut::create(1.0f), NULL));
	}
}

/*
* AI走棋
*/
void GameScene::goWithAI()
{
	if (m_selectPiece)
	{
		m_selectPiece->getSprite()->stopAllActions();
		m_selectPiece = nullptr;
	}

	//AI移动到的位置
	int runInx;

	//AI走的棋子
	Stc_Piece*  stc_piece = nullptr;

	m_AI.goStep(stc_piece, runInx);

	for (vector<Piece>::iterator it = m_vecPiece.begin(); it != m_vecPiece.end(); it++)
	{
		if (it->getData() == stc_piece)
		{
			m_selectPiece = &*it;
			break;
		}
	}

	if (m_selectPiece == nullptr)
	{
		m_winSprite->setVisible(true);
		return;
	}

	m_selectPiece->getSprite()->setColor(Color3B(128, 0, 128));
	drawCanFallPoint();

	this->schedule([this,runInx](float delta)
	{
		this->unschedule("test");
		movePiece(runInx, m_ruleCenter.myCamp == PLAYER_RED ? PLAYER_BLACK : PLAYER_RED);
		m_selectPiece->getSprite()->setColor(Color3B(255, 255, 255));
		m_phase = PH_WAIT_SELECT;
	}, 1.0f,"test");	
}

/*
* 触摸事件
* 我方落子状态：点击棋子显示方框、点击其它位置隐藏方框
* 我方选中棋子状态：点击可落子位置，移动棋子并隐藏方框、点击其它位置隐藏方框
* 对方操作状态：不处理
*/
bool GameScene::onTouchBegan(Touch * t, Event *e)
{
	switch (m_phase)
	{
	case PH_WAIT_OTHER_GO:	//等待对方落子
		break;
	case PH_WAIT_SELECT:	//等待我方选择棋子
	{
		float x = t->getLocation().x;
		float y = t->getLocation().y;
		Vec2 piecePos;
		vector<Piece>::iterator it = m_vecPiece.begin();

		for (; it != m_vecPiece.end(); it++)
		{
			if (m_selectPiece)
			{
				m_selectPiece->getSprite()->stopAllActions();
				m_selectPiece->getSprite()->setVisible(true);
			}

			//只有自己的棋子才能点击
			if (it->getData()->player != m_ruleCenter.myCamp)
			{
				continue;
			}
			//只有活着的棋子才能点击
			if (it->getData()->isAlive == false)
			{
				continue;
			}
			//转换为屏幕坐标
			piecePos = transferPosition(it->getData()->player == m_ruleCenter.myCamp, it->getData()->logicPos[0], it->getData()->logicPos[1]);
			//找到点击的棋子
			if (abs(piecePos.x - x) <= piece_radius && abs(piecePos.y - y) <= piece_radius)
			{
				//显示选中框
				m_pieceBox->setVisible(true);
				m_pieceBox->setPosition(it->getSprite()->getPosition());
				//设置选中棋子
				m_selectPiece = (Piece*)&*it;
				//绘制选中棋子可走位置
				drawCanFallPoint();
				//进入等待我方落子状态
				m_phase = PH_WAIT_GO;
				break;
			}
		}

		//没有找到点击的棋子
		if (it == m_vecPiece.end())
		{
			//隐藏选中框
			m_pieceBox->setVisible(false);
			//清除选中棋子
			m_selectPiece = nullptr;
			//清除可落子点
			m_drawNode->clear();
		}

		break;
	}
	case PH_WAIT_GO:	//等待我方落子
	{
		float x = t->getLocation().x;
		float y = t->getLocation().y;
		Vec2 piecePos;
		//遍历可以落子的位置
		bool bFind = false;
		for (int i = 0; i < MAX_FALL_COUNT; i++)
		{
			if (m_selectPiece->getData()->canFallPos[i][0] == 255)
			{
				break;
			}
			piecePos = transferPosition(true, m_selectPiece->getData()->canFallPos[i][0], m_selectPiece->getData()->canFallPos[i][1]);
			//找到位置
			if (abs(x - piecePos.x) < piece_radius && abs(y - piecePos.y) < piece_radius)
			{
				int tempX = m_selectPiece->getData()->logicPos[0];
				int tempY = m_selectPiece->getData()->logicPos[1];
				//临时更新位置以判断能否被将军
				m_selectPiece->updatePos(m_selectPiece->getData()->canFallPos[i][0], m_selectPiece->getData()->canFallPos[i][1]);

				//落子后必须保证我方不被将军
				if (!m_ruleCenter.canJiangJu(m_ruleCenter.myCamp == PLAYER_RED ? PLAYER_BLACK : PLAYER_BLACK))
				{
					//还原位置
					m_selectPiece->updatePos(tempX, tempY);
					//移动棋子到落子位置
					movePiece(i, m_ruleCenter.myCamp);
					//隐藏选中框
					m_pieceBox->setVisible(false);
					//进入等待对方落子阶段
					m_phase = PH_WAIT_OTHER_GO;
					//AI走棋
					goWithAI();
				}
				else
				{
					//还原位置
					m_selectPiece->updatePos(tempX, tempY);
					//提醒玩家拯救将军
					m_warnText->runAction(Sequence::create(FadeIn::create(0.3f), FadeOut::create(1.0f)));
				}

				bFind = true;
				break;
			}
		}

		//判断是否选择另一个棋子
		if (!bFind)
		{
			for (vector<Piece>::iterator it = m_vecPiece.begin(); it != m_vecPiece.end(); it++)
			{
				//只有自己的棋子才能点击
				if (it->getData()->player != m_ruleCenter.myCamp)
				{
					continue;
				}
				//只有活着的棋子才能点击
				if (it->getData()->isAlive == false)
				{
					continue;
				}
				//转换为屏幕坐标
				piecePos = transferPosition(it->getData()->player == m_ruleCenter.myCamp, it->getData()->logicPos[0], it->getData()->logicPos[1]);
				//找到点击的棋子
				if (abs(piecePos.x - x) <= piece_radius && abs(piecePos.y - y) <= piece_radius)
				{
					//显示选中框
					m_pieceBox->setVisible(true);
					m_pieceBox->setPosition(it->getSprite()->getPosition());
					//设置选中棋子
					m_selectPiece = (Piece*)&*it;
					//绘制选中棋子可落子位置
					drawCanFallPoint();
					bFind = true;
					break;
				}
			}
		}
		//没找到位置
		if (!bFind)
		{
			//隐藏选中框
			m_pieceBox->setVisible(false);
			//清除选中棋子
			m_selectPiece = nullptr;
			//清除可落子点
			m_drawNode->clear();
			//进入等待选择阶段
			m_phase = PH_WAIT_SELECT;
		}
		break;
	}
	default:
		break;
	}
	return false;
}

/*
* 将逻辑位置转换成屏幕位置
*/
Vec2 GameScene::transferPosition(bool isMe, int x, int y)
{
	Vec2 pos;
	//我方棋子，x坐标从右往左依次是1-9，y坐标从下往上依次是1-10
	if (isMe)
	{
		pos.x = piece_start_x + (9 - x)*piece_step_x;
		pos.y = piece_start_y + (y - 1)*piece_step_y;
	}
	//对方棋子，x坐标从左往右依次是1-9，y坐标从上往下依次是1-10
	else
	{
		pos.x = piece_start_x + (x - 1)*piece_step_x;
		pos.y = piece_start_y + (10 - y)*piece_step_y;
	}
	return pos;
}

/*
* 绘制可以落子的位置点
*/
void GameScene::drawCanFallPoint()
{
	if (m_selectPiece == nullptr || m_drawNode == nullptr)
	{
		return;
	}

	m_drawNode->clear();

	//查找可以落子的位置
	m_ruleCenter.findCallFallPosition(m_selectPiece->getData());

	//绘制可以落子的位置
	Vec2 pos;
	for (int i = 0; i < MAX_FALL_COUNT; i++)
	{
		if (m_selectPiece->getData()->canFallPos[i][0] == 255)
		{
			break;
		}
		pos = transferPosition(m_selectPiece->getData()->player == m_ruleCenter.myCamp, m_selectPiece->getData()->canFallPos[i][0], m_selectPiece->getData()->canFallPos[i][1]);
		m_drawNode->setLineWidth(5);
		m_drawNode->drawCircle(pos, piece_radius, 0, 60, false, m_selectPiece->getData()->player == PLAYER_RED ? Color4F(1.0f, 0, 0, 1.0f) : Color4F(0, 0, 0, 1.0f));
	}
}
