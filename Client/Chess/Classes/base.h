#ifndef __BASE_H__
#define __BASE_H__
/**
*
* 基础数据
* 描述：定义基础数据和宏
* 作者：林锦新
* 日期：2016年2月19日
*/

#define WRITEABLEPATH (FileUtils::getInstance()->getWritablePath().c_str())
#define OUTLOG 1

static int start_x;	//棋盘开始位置X
static int start_y;	//棋盘开始位置Y
static int step_x;	//棋盘格子大小X
static int step_y;	//棋盘格子大小Y
static int piece_r;	//棋子半径

#endif // !__BASE_H__

