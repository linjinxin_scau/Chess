<GameFile>
  <PropertyGroup Name="MainLayer" Type="Layer" ID="5c64a433-12ab-4bc0-b3ed-65f479dcd0b1" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Layer" Tag="132" ctype="GameLayerObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="sp_bg" ActionTag="-1107500788" Tag="133" IconVisible="False" LeftMargin="80.0000" RightMargin="80.0000" TopMargin="56.0000" BottomMargin="56.0000" ctype="SpriteObjectData">
            <Size X="480.0000" Y="848.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.3300" ScaleY="1.1400" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7500" Y="0.8833" />
            <FileData Type="Normal" Path="pic/menubg.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_bg2" ActionTag="-14849750" Tag="134" IconVisible="False" LeftMargin="80.0000" RightMargin="80.0000" TopMargin="60.0000" BottomMargin="60.0000" ctype="SpriteObjectData">
            <Size X="480.0000" Y="840.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.3300" ScaleY="1.1400" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7500" Y="0.8750" />
            <FileData Type="Normal" Path="pic/menubg2.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_logo" ActionTag="988776845" Tag="135" IconVisible="False" LeftMargin="150.5298" RightMargin="27.4702" TopMargin="105.6505" BottomMargin="513.3495" ctype="SpriteObjectData">
            <Size X="462.0000" Y="341.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="381.5298" Y="683.8495" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5961" Y="0.7123" />
            <PreSize X="0.7219" Y="0.3552" />
            <FileData Type="MarkedSubImage" Path="pic/title.png" Plist="ui.plist" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_start" ActionTag="-1435635121" Tag="136" IconVisible="False" LeftMargin="216.5000" RightMargin="216.5000" TopMargin="404.6300" BottomMargin="505.3700" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="177" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="207.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="530.3700" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5525" />
            <PreSize X="0.3234" Y="0.0521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="pic/menu_start_1.png" Plist="ui.plist" />
            <PressedFileData Type="MarkedSubImage" Path="pic/menu_start_2.png" Plist="ui.plist" />
            <NormalFileData Type="MarkedSubImage" Path="pic/menu_start_1.png" Plist="ui.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_about" ActionTag="-597252620" Tag="137" IconVisible="False" LeftMargin="216.5001" RightMargin="216.4999" TopMargin="722.8883" BottomMargin="187.1117" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="177" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="207.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0001" Y="212.1117" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.2209" />
            <PreSize X="0.3234" Y="0.0521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="pic/menu_about_1.png" Plist="ui.plist" />
            <PressedFileData Type="MarkedSubImage" Path="pic/menu_about_2.png" Plist="ui.plist" />
            <NormalFileData Type="MarkedSubImage" Path="pic/menu_about_1.png" Plist="ui.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_exit" ActionTag="-987624852" Tag="138" IconVisible="False" LeftMargin="216.5000" RightMargin="216.5000" TopMargin="802.4509" BottomMargin="107.5491" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="177" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="207.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.6545" ScaleY="0.3026" />
            <Position X="351.9815" Y="122.6791" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5500" Y="0.1278" />
            <PreSize X="0.3234" Y="0.0521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="pic/menu_exit_1.png" Plist="ui.plist" />
            <PressedFileData Type="MarkedSubImage" Path="pic/menu_exit_2.png" Plist="ui.plist" />
            <NormalFileData Type="MarkedSubImage" Path="pic/menu_exit_1.png" Plist="ui.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_goon" ActionTag="-467084936" Tag="139" IconVisible="False" LeftMargin="216.4977" RightMargin="216.5023" TopMargin="484.1953" BottomMargin="425.8047" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="177" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="207.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.4758" ScaleY="0.4314" />
            <Position X="314.9883" Y="447.3747" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4922" Y="0.4660" />
            <PreSize X="0.3234" Y="0.0521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="pic/menu_goon_1.png" Plist="ui.plist" />
            <PressedFileData Type="MarkedSubImage" Path="pic/menu_goon_2.png" Plist="ui.plist" />
            <NormalFileData Type="MarkedSubImage" Path="pic/menu_goon_1.png" Plist="ui.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_help" ActionTag="1285673940" Tag="140" IconVisible="False" LeftMargin="216.5001" RightMargin="216.4999" TopMargin="643.3238" BottomMargin="266.6762" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="177" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="207.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0001" Y="291.6762" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3038" />
            <PreSize X="0.3234" Y="0.0521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="pic/menu_help_1.png" Plist="ui.plist" />
            <PressedFileData Type="MarkedSubImage" Path="pic/menu_help_2.png" Plist="ui.plist" />
            <NormalFileData Type="MarkedSubImage" Path="pic/menu_help_1.png" Plist="ui.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_set" ActionTag="-1372714150" Tag="141" IconVisible="False" LeftMargin="216.5001" RightMargin="216.4999" TopMargin="563.7595" BottomMargin="346.2405" TouchEnable="True" FontSize="14" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="177" Scale9Height="28" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="207.0000" Y="50.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0001" Y="371.2405" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.3867" />
            <PreSize X="0.3234" Y="0.0521" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="MarkedSubImage" Path="pic/menu_set_1.png" Plist="ui.plist" />
            <PressedFileData Type="MarkedSubImage" Path="pic/menu_set_2.png" Plist="ui.plist" />
            <NormalFileData Type="MarkedSubImage" Path="pic/menu_set_1.png" Plist="ui.plist" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>