<GameFile>
  <PropertyGroup Name="GameScene" Type="Scene" ID="e0f186dc-5229-42a3-931a-b258f7043d03" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="55" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="sp_bg" ActionTag="1593374095" Tag="77" IconVisible="False" ctype="SpriteObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="style_2/bg.png" Plist="" />
            <BlendFunc Src="770" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_board" ActionTag="635808295" Tag="79" IconVisible="False" TopMargin="121.5000" BottomMargin="121.5000" ctype="SpriteObjectData">
            <Size X="640.0000" Y="717.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="0.7469" />
            <FileData Type="Normal" Path="style_2/board.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_regret" ActionTag="1793284294" Tag="69" IconVisible="False" LeftMargin="530.9920" RightMargin="29.0080" TopMargin="856.0968" BottomMargin="67.9032" TouchEnable="True" FontSize="36" ButtonText="悔棋" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="80.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.4876" ScaleY="0.6138" />
            <Position X="570.0000" Y="90.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8906" Y="0.0938" />
            <PreSize X="0.1250" Y="0.0375" />
            <FontResource Type="Normal" Path="art.ttf" Plist="" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="btn_exit" ActionTag="-89513679" Tag="70" IconVisible="False" LeftMargin="530.0000" RightMargin="30.0000" TopMargin="904.0000" BottomMargin="20.0000" TouchEnable="True" FontSize="36" ButtonText="退出" Scale9Enable="True" LeftEage="15" RightEage="15" TopEage="11" BottomEage="11" Scale9OriginX="15" Scale9OriginY="11" Scale9Width="16" Scale9Height="14" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="ButtonObjectData">
            <Size X="80.0000" Y="36.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="570.0000" Y="38.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.8906" Y="0.0396" />
            <PreSize X="0.1250" Y="0.0375" />
            <FontResource Type="Normal" Path="art.ttf" Plist="" />
            <TextColor A="255" R="65" G="65" B="70" />
            <DisabledFileData Type="Default" Path="Default/Button_Disable.png" Plist="" />
            <PressedFileData Type="Default" Path="Default/Button_Press.png" Plist="" />
            <NormalFileData Type="Default" Path="Default/Button_Normal.png" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
          <AbstractNodeData Name="text_jiang" ActionTag="864283246" Tag="12" IconVisible="False" LeftMargin="272.0000" RightMargin="272.0000" TopMargin="440.0000" BottomMargin="420.0000" FontSize="100" LabelText="将" ShadowOffsetX="4.0000" ShadowOffsetY="4.0000" ShadowEnabled="True" ctype="TextObjectData">
            <Size X="96.0000" Y="100.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="470.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="0" G="0" B="0" />
            <PrePosition X="0.5000" Y="0.4896" />
            <PreSize X="0.1500" Y="0.1042" />
            <FontResource Type="Normal" Path="art.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="77" G="77" B="77" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_win" ActionTag="-630132967" VisibleForFrame="False" Tag="12" IconVisible="False" LeftMargin="112.0000" RightMargin="112.0000" TopMargin="358.0000" BottomMargin="398.0000" ctype="SpriteObjectData">
            <Size X="416.0000" Y="204.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5208" />
            <PreSize X="0.6500" Y="0.2125" />
            <FileData Type="Normal" Path="pic/win.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_lost" ActionTag="456519004" VisibleForFrame="False" Tag="13" IconVisible="False" LeftMargin="112.0000" RightMargin="112.0000" TopMargin="358.0000" BottomMargin="398.0000" ctype="SpriteObjectData">
            <Size X="416.0000" Y="204.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5208" />
            <PreSize X="0.6500" Y="0.2125" />
            <FileData Type="Normal" Path="pic/lost.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="text_warn" ActionTag="-657440791" Tag="14" IconVisible="False" LeftMargin="92.5000" RightMargin="92.5000" TopMargin="456.0000" BottomMargin="456.0000" FontSize="48" LabelText="请不要放弃我方将军！" ShadowOffsetX="2.0000" ShadowOffsetY="-2.0000" ctype="TextObjectData">
            <Size X="455.0000" Y="48.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="0.7109" Y="0.0500" />
            <FontResource Type="Normal" Path="art.ttf" Plist="" />
            <OutlineColor A="255" R="255" G="0" B="0" />
            <ShadowColor A="255" R="110" G="110" B="110" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>