<GameFile>
  <PropertyGroup Name="LoadScene" Type="Scene" ID="e0912e55-4503-4821-83c3-fd94572d4f1c" Version="3.10.0.0" />
  <Content ctype="GameProjectContent">
    <Content>
      <Animation Duration="0" Speed="1.0000" />
      <ObjectData Name="Scene" Tag="24" ctype="GameNodeObjectData">
        <Size X="640.0000" Y="960.0000" />
        <Children>
          <AbstractNodeData Name="sp_bg" ActionTag="1884735116" Tag="26" IconVisible="False" ctype="SpriteObjectData">
            <Size X="640.0000" Y="960.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="480.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5000" />
            <PreSize X="1.0000" Y="1.0000" />
            <FileData Type="Normal" Path="pic/loadbg.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_loadword" ActionTag="-395018728" Tag="35" IconVisible="False" LeftMargin="257.0000" RightMargin="257.0000" TopMargin="366.5000" BottomMargin="566.5000" ctype="SpriteObjectData">
            <Size X="126.0000" Y="27.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0000" Y="580.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.6042" />
            <PreSize X="0.1969" Y="0.0281" />
            <FileData Type="Normal" Path="pic/loadword.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="sp_load" ActionTag="1238939684" Tag="37" IconVisible="False" LeftMargin="83.5002" RightMargin="83.4998" TopMargin="432.0000" BottomMargin="472.0000" ctype="SpriteObjectData">
            <Size X="473.0000" Y="56.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="320.0002" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.5000" Y="0.5208" />
            <PreSize X="0.7391" Y="0.0583" />
            <FileData Type="Normal" Path="pic/loadrim.png" Plist="" />
            <BlendFunc Src="1" Dst="771" />
          </AbstractNodeData>
          <AbstractNodeData Name="bar_load" ActionTag="-2021526159" Tag="34" IconVisible="False" LeftMargin="108.5000" RightMargin="132.5000" TopMargin="454.5000" BottomMargin="494.5000" ProgressInfo="0" ctype="LoadingBarObjectData">
            <Size X="399.0000" Y="11.0000" />
            <AnchorPoint ScaleX="0.5000" ScaleY="0.5000" />
            <Position X="308.0000" Y="500.0000" />
            <Scale ScaleX="1.0000" ScaleY="1.0000" />
            <CColor A="255" R="255" G="255" B="255" />
            <PrePosition X="0.4812" Y="0.5208" />
            <PreSize X="0.6234" Y="0.0115" />
            <ImageFileData Type="Normal" Path="pic/load.png" Plist="" />
          </AbstractNodeData>
        </Children>
      </ObjectData>
    </Content>
  </Content>
</GameFile>